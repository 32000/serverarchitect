package ladeira.vanish;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import ladeira.general.MSGType;
import ladeira.general.MessageHandler;
import ladeira.general.ServerHandler;

public class cVanish {
	Plugin plugin;
	
	public cVanish() {
		plugin = ServerHandler.getPlugin();
	}
	
	public void onVanishCommand(Player p) { 
		if (p.hasPermission("Vanish.use") || p.isOp()) { // Check if the player has the required permissions
			if (!VanishHandler.isVanished(p)) {	// If player isn't vanished already
				VanishHandler.vanishPlayer(p);
				p.sendMessage(MessageHandler.format("You have vanished", MSGType.info));
			} else {
				p.sendMessage(MessageHandler.format("You are already vanished", MSGType.error)); // Is already vanished
			}
		} else {
			p.sendMessage(MessageHandler.format("You do not have the permission Vanish.use", MSGType.error)); // Doesn't have required permissions
		}
	}
	
	public void onUnvanishCommand(Player p) {
		if (p.hasPermission("Vanish.use") || p.isOp()) { // Check if the player has the required permissions
			if (VanishHandler.isVanished(p)) { // If player is vanished
				VanishHandler.unvanishPlayer(p);
				p.sendMessage(MessageHandler.format("You have unvanished", MSGType.info));
			} else {
				p.sendMessage(MessageHandler.format("You are not vanished", MSGType.error)); // Not vanished
			}
		} else {
			p.sendMessage(MessageHandler.format("You do not have the permission Vanish.use", MSGType.error)); // Doesn't have required permissions
		}
	}
}