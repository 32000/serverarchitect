package ladeira.vanish;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import ladeira.attributes.PermissionHandler;
import ladeira.versions.VersionHandler;

public class VanishInv {
	
	private ItemStack compass; // Compass used to teleport to players
	private ItemStack wool; // Wool used to switch gamemodes between spectator and creative
	Inventory inv; // Teleport inventory
	
	private String inventoryName;
	VersionHandler versions;
	
	public VanishInv() {
		inv = Bukkit.createInventory(null, 9, ChatColor.RED + "If you see this an error has occured"); // To prevent NullPointerException when .equals() is called on the inventory
		inventoryName = ChatColor.RED + "Teleport";
		versions = new VersionHandler();
		loadItems();
	}
	
	public void setInv(Player p) {
		System.out.println(compass.toString());
		// Set toolbar slots
		p.getInventory().setItem(7, wool);
		p.getInventory().setItem(8, compass);
		
		p.updateInventory(); // Update player's inventory
	}
	
	public void loadItems() { // Load the item variables with actual items
		compass = new ItemStack(Material.COMPASS);
		wool = versions.getGreenWool();
		ItemMeta cMeta = compass.getItemMeta();
		ItemMeta sMeta = wool.getItemMeta();
		
		cMeta.setDisplayName(ChatColor.RED + "Player Teleporter");
		compass.setItemMeta(cMeta);
		
		sMeta.setDisplayName(ChatColor.RED + "Gamemode Switcher");
		wool.setItemMeta(sMeta);
		
	}
	
	public boolean isCompass(ItemStack stack) {
		return compass.equals(stack);
	}
	
	public boolean isWool(ItemStack stack) {
		return wool.equals(stack);
	}
	
	public String getInvName() {
		return inventoryName;
	}
	
	@SuppressWarnings("deprecation")
	public void openTeleportInventory(Player p) {
		int playerCount = Bukkit.getOnlinePlayers().size();
		
		if (playerCount <= 9) {
			inv = Bukkit.createInventory(null, 9, inventoryName);
		} else if (playerCount <= 18) {
			inv = Bukkit.createInventory(null, 18, inventoryName);
		} else if (playerCount <= 27) {
			inv = Bukkit.createInventory(null, 27, inventoryName);
		} else if (playerCount <= 36) {
			inv = Bukkit.createInventory(null, 36, inventoryName);
		} else if (playerCount <= 45) {
			inv = Bukkit.createInventory(null, 45, inventoryName);
		} else if (playerCount <= 54) {
			inv = Bukkit.createInventory(null, 54, inventoryName);
		} else {
			Bukkit.getLogger().severe("If you see this message it is because the world has ended, Russia has declared war on the US, the world is on fire, Germany has declared war on France, France has surrenderd and Winston Churchill has risen from the dead. If this happened please pray for me, and for my forgiveness, Amen.");
			return;
		}
		
		int i = 0;
		for (Player online : Bukkit.getOnlinePlayers()) {
			ItemStack skull = new ItemStack(versions.getSkull(), 1, (byte) 3);
			SkullMeta meta = (SkullMeta) skull.getItemMeta();
			 
			meta.setOwner(online.getName());
			meta.setDisplayName(PermissionHandler.formatName(online.getName(), online.getUniqueId()));
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(ChatColor.DARK_RED + "Distance: " + ChatColor.WHITE + p.getLocation().distance(online.getLocation()));
			meta.setLore(lore);
			skull.setItemMeta(meta);
			inv.setItem(i, skull);
			i++;
		}
		p.openInventory(inv);
	}
	
	public void swapGamemodes(Player p) {
		if (p.getGameMode() == GameMode.CREATIVE) { // If creative then spec
			p.setGameMode(GameMode.SPECTATOR);
		} else { // If the game mode is not creative make it creative
			p.setGameMode(GameMode.CREATIVE);
		}
	}
}