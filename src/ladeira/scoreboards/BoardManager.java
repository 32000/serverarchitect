package ladeira.scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class BoardManager {
	
	private ScoreboardManager manager;
	Scoreboard board;
	Objective objective;
	
	public BoardManager() {
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
	}
	
	@SuppressWarnings("deprecation")
	public void registerObjective(String name) {
		objective = board.registerNewObjective("test", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(name);
	}
}
