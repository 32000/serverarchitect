package ladeira.attributes;

import java.util.HashSet;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import ladeira.general.ServerHandler;

public class AttributesHandler {
	
	private static HashSet<Permission> permissions = new HashSet<Permission>();
	
	public static void loadAttributes() {
		loadPermissions();
	}
	
	public static Permission findPerm(int id) {
		for (Permission i: permissions) {
			if (i.getID() == id) {
				return i;
			}
		}
		return null;
	}
	private static void loadPermissions() {
		FileConfiguration config = ServerHandler.getAttrConfig();
		int id;
		ChatColor color;
		ChatColor chatColor;
		for (String key : config.getConfigurationSection("permissions").getKeys(false)) {
			id = config.getInt("permissions." + key + ".id");
			color = getChatColor(config.getString("permissions." + key + ".nameColor"));
			chatColor = getChatColor(config.getString("permissions." + key + ".chatColor"));
			
			permissions.add(new Permission(id, key, "permissions." + key, color, chatColor));
		}
	}
	
	private static ChatColor getChatColor(String text) {
		try {
			return ChatColor.valueOf(text.toUpperCase());
		} catch (IllegalArgumentException e) {
			ServerHandler.getPlugin().getLogger().severe("Got IllegalArgumentException when loading the chatcolor");
		}
		return ChatColor.WHITE;
	}
}