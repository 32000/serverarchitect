package ladeira.attributes;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import ladeira.general.ServerHandler;

public class PermissionHandler {
	
	private static HashMap<UUID,PermissionAttachment> map = new HashMap<UUID,PermissionAttachment>();
	
	public static String formatName(String msg, UUID id) {
		int permLevel = ServerHandler.getPlayerConfig().getInt(id.toString() + ".permLevel");
		Permission perm = AttributesHandler.findPerm(permLevel);
		if (perm == null) { ServerHandler.getPlugin().getLogger().severe("Invalid perm level: " + permLevel); return msg; }
		
		msg = perm.getColor() + msg;
		return msg;
	}
	
	public static String formatChat(String msg, UUID id) {
		int permLevel = ServerHandler.getPlayerConfig().getInt(id.toString() + ".permLevel");
		Permission perm = AttributesHandler.findPerm(permLevel);
		if (perm == null) { ServerHandler.getPlugin().getLogger().severe("Invalid perm level: " + permLevel); return msg; }
		
		msg = perm.getChatColor() + msg;
		return msg;
	}
	
	public static void loadPlayerPermissions(Player p) {
		FileConfiguration attrConfig = ServerHandler.getAttrConfig();
		FileConfiguration playerConfig = ServerHandler.getPlayerConfig();
		UUID id = p.getUniqueId();
		int permLevel = playerConfig.getInt(id + ".permLevel");
		
		PermissionAttachment attachment = p.addAttachment(ServerHandler.getPlugin());
		map.put(p.getUniqueId(), attachment);
		
		for (String perm : playerConfig.getStringList(id + ".permissions")) {
			attachment.setPermission(perm, true);
		}
	
		for (String perm : attrConfig.getStringList(AttributesHandler.findPerm(permLevel).getPath() + ".permissions")) {
			attachment.setPermission(perm, true);
		}
	}
	
	public static void unloadPlayerPermissions(Player p) {
		p.removeAttachment(map.get(p.getUniqueId()));
		map.remove(p.getUniqueId());
	}
}