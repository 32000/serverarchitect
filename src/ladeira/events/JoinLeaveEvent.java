package ladeira.events;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import ladeira.attributes.PermissionHandler;
import ladeira.general.MSGType;
import ladeira.general.MessageHandler;
import ladeira.general.ServerHandler;
import ladeira.vanish.VanishHandler;

public class JoinLeaveEvent implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void JoinEvent(PlayerJoinEvent e) throws IOException {
		e.setJoinMessage(""); // Remove default joinMessage
		joinMessage(true, e.getPlayer());
		setDefaultUUID(e.getPlayer().getUniqueId()); // Add the player into players.yml
		PermissionHandler.loadPlayerPermissions(e.getPlayer()); // Add permission attachments, see PermissionHandler for more info
		
		for (UUID id : VanishHandler.getMap()) {
			e.getPlayer().hidePlayer(Bukkit.getPlayer(id)); // Hide already vanished players
		}
	}

	public void LeaveEvent(PlayerQuitEvent e) {
		e.setQuitMessage(""); // Remove default leaveMessage
		joinMessage(false, e.getPlayer());
		PermissionHandler.unloadPlayerPermissions(e.getPlayer()); // Remove permission attachments, see PermissionHandler for more info
	}
	
	public void joinMessage(boolean change, Player p) {
		String msg = "";
		if (change) {
			msg = ServerHandler.getMSGConfig().getString("join");
		} else {
			msg = ServerHandler.getMSGConfig().getString("leave");
		}
		msg = msg.replace("[player]", PermissionHandler.formatName(p.getName(), p.getUniqueId()));
		Bukkit.broadcastMessage(MessageHandler.format(msg, MSGType.info));
	}
	
	public void setDefaultUUID(UUID id) throws IOException {
		FileConfiguration config = ServerHandler.getPlayerConfig();
		
		config.addDefault(id.toString() + ".permLevel", 0);
		config.options().copyDefaults(true);
		config.save(ServerHandler.getPlayerFile());
	}
}