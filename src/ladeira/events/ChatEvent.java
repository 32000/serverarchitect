package ladeira.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
//import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;

import ladeira.attributes.PermissionHandler;
import ladeira.general.MSGType;
import ladeira.general.MessageHandler;
import ladeira.vanish.cVanish;

@SuppressWarnings("deprecation")
public class ChatEvent implements Listener {

	@EventHandler
	public void onPlayerChat(PlayerChatEvent e) {
		if (e.getMessage().toCharArray()[0] == '+') { // If it's the custom prefix
			Player p = e.getPlayer();
			String msg[] = e.getMessage().split(" "); // Split the message into arguments
			cVanish vanish = new cVanish(); // Get the Vanish command object
			System.out.println(p.getName() + " has executed the command " + msg[0].toLowerCase());
			if (msg[0].equalsIgnoreCase("+vanish")) { // Vanish command
				vanish.onVanishCommand(p);
				e.setCancelled(true);
				return;
			} else if (msg[0].equalsIgnoreCase("+unvanish")) { // Unvanish command
				vanish.onUnvanishCommand(p);
				e.setCancelled(true);
				return;
			}
			p.sendMessage(MessageHandler.format("That is not a valid command", MSGType.error));
			e.setCancelled(true);
			return;
		}
		Bukkit.broadcastMessage(getMessage(e));
		e.setCancelled(true);
	}
	
	public String getMessage(PlayerChatEvent e) { // TODO Merge getPermName, getPermMSG and getMessage
		String msg = "";
		Player p = e.getPlayer();
		msg = MessageHandler.format(getPermName(p) + ": " + getPermMSG(p, e.getMessage()), MSGType.chat); // TODO Custom chat formatting
		
		return msg;
	}
	
	public String getPermName(Player p) { return PermissionHandler.formatName(p.getName(), p.getUniqueId()); }
	
	public String getPermMSG(Player p, String msg) { return PermissionHandler.formatChat(msg,p.getUniqueId()); }
}