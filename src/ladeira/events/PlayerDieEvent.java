package ladeira.events;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;

import ladeira.general.MSGType;
import ladeira.general.MessageHandler;
import ladeira.general.ServerHandler;
import ladeira.versions.VersionHandler;

public class PlayerDieEvent implements Listener {
	
	@EventHandler
	public void onPlayerdeath(PlayerDeathEvent e) {
		e.setDeathMessage(""); // Disable default death Bukkit.broadcastMessage(Message
		// if (!(e.getEntity() instanceof Player)) { return; } // Check if it's a player (need?)
		
		Player p = (Player) e.getEntity();
		FileConfiguration config = ServerHandler.getMSGConfig();
		EntityDamageEvent de = p.getLastDamageCause(); // Gets damage event
		DamageCause cause = de.getCause(); // Gets damage cause
		broadcastMessage(config, cause, p);
	}

	@SuppressWarnings("incomplete-switch")
	public void broadcastMessage(FileConfiguration config, DamageCause cause, Player p) {
		VersionHandler handler = new VersionHandler();
		
		if (handler.getVersion() > 1.12) {
			switch (cause) {
			case BLOCK_EXPLOSION:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.tnt"), MSGType.death, p));
				break;
			case CONTACT:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.cactus"), MSGType.death, p));
				break;
			case CRAMMING:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.cramming"), MSGType.death, p));
				break;
			case CUSTOM:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.default"), MSGType.death, p));
				break;
			case DRAGON_BREATH:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.dragon"), MSGType.death, p));
				break;
			case DROWNING:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.drowning"), MSGType.death, p));
				break;
			case DRYOUT:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.default"), MSGType.death, p));
				break;
			case ENTITY_ATTACK:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.mob"), MSGType.death, p));
				break;
			case ENTITY_EXPLOSION:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.creeper"), MSGType.death, p));
				break;
			case ENTITY_SWEEP_ATTACK:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.mob"), MSGType.death, p));
				break;
			case FALL:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.fall"), MSGType.death, p));
				break;
			case FALLING_BLOCK:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.anvil"), MSGType.death, p));
				break;
			case FIRE:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.fire"), MSGType.death, p));
				break;
			case FIRE_TICK:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.burning"), MSGType.death, p));
				break;
			case FLY_INTO_WALL:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.elytra"), MSGType.death, p));
				break;
			case HOT_FLOOR:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.magma-block"), MSGType.death, p));
				break;
			case LAVA:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.lava"), MSGType.death, p));
				break;
			case LIGHTNING:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.lightning"), MSGType.death, p));
				break;
			case MAGIC:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.potion"), MSGType.death, p));
				break;
			case MELTING:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.default"), MSGType.death, p));
				break;
			case POISON:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.potion"), MSGType.death, p));
				break;
			case PROJECTILE:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.projectile"), MSGType.death, p));
				break;
			case STARVATION:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.starvation"), MSGType.death, p));
				break;
			case SUFFOCATION:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.suffocation"), MSGType.death, p));
				break;
			case SUICIDE:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.default"), MSGType.death, p));
				break;
			case THORNS:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.thorns"), MSGType.death, p));
				break;
			case VOID:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.void"), MSGType.death, p));
				break;
			case WITHER:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.potion"), MSGType.death, p));
				break;
			}
		} else {
			switch (cause) {
			case BLOCK_EXPLOSION:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.tnt"), MSGType.death, p));
				break;
			case CONTACT:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.cactus"), MSGType.death, p));
				break;
			case CRAMMING:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.cramming"), MSGType.death, p));
				break;
			case CUSTOM:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.default"), MSGType.death, p));
				break;
			case DRAGON_BREATH:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.dragon"), MSGType.death, p));
				break;
			case DROWNING:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.drowning"), MSGType.death, p));
				break;
			case ENTITY_ATTACK:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.mob"), MSGType.death, p));
				break;
			case ENTITY_EXPLOSION:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.creeper"), MSGType.death, p));
				break;
			case ENTITY_SWEEP_ATTACK:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.mob"), MSGType.death, p));
				break;
			case FALL:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.fall"), MSGType.death, p));
				break;
			case FALLING_BLOCK:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.anvil"), MSGType.death, p));
				break;
			case FIRE:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.fire"), MSGType.death, p));
				break;
			case FIRE_TICK:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.burning"), MSGType.death, p));
				break;
			case FLY_INTO_WALL:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.elytra"), MSGType.death, p));
				break;
			case HOT_FLOOR:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.magma-block"), MSGType.death, p));
				break;
			case LAVA:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.lava"), MSGType.death, p));
				break;
			case LIGHTNING:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.lightning"), MSGType.death, p));
				break;
			case MAGIC:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.potion"), MSGType.death, p));
				break;
			case MELTING:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.default"), MSGType.death, p));
				break;
			case POISON:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.potion"), MSGType.death, p));
				break;
			case PROJECTILE:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.projectile"), MSGType.death, p));
				break;
			case STARVATION:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.starvation"), MSGType.death, p));
				break;
			case SUFFOCATION:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.suffocation"), MSGType.death, p));
				break;
			case SUICIDE:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.default"), MSGType.death, p));
				break;
			case THORNS:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.thorns"), MSGType.death, p));
				break;
			case VOID:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.void"), MSGType.death, p));
				break;
			case WITHER:
				Bukkit.broadcastMessage(MessageHandler.format(config.getString("death.potion"), MSGType.death, p));
				break;
			}
		}
	}
}