package ladeira.versions;

import org.bukkit.entity.Player;

public interface VersionClass {
	
	public void showPlayer(Player p, Player show);
	public void hidePlayer(Player p, Player hide);
}
